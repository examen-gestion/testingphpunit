<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class Tests extends TestCase
{

    //DATOS PARA REALIZAR LOS TEST
    public function data($option)
    {
        $data = array(
            "id_card" => 109290322,
            "name" => "Yorleni",
            "last_name" => "Varela",
            "email" => "yorlevarela21@gmail.com",
            "phone_number" => "84801379",
            "location" => "Camuro",
            "payer" => "Yorleni",
            "comments" => "Sensibilidad",
        );

        if ($option === 'create') {
            return json_encode($data);
        }
    }

    //TEST 1 - VALIDATE NAME
    public function testValidateName()
    {
        $form = $this->data("create");
        $array = json_decode($form);
        $array->name = "Y0rl3n1";
        $form = json_encode($array);
        $client = new Client();
        $response = $client->post('http://localhost/backend/patient/create', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $form
        ]);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals($body[0], "record name only accepts letters");
    }

    //TEST 2 - VALIDATE LASTNAME
    public function testValidateLastName()
    {
        $form = $this->data("create");
        $array = json_decode($form);
        $array->last_name = "Var#la";
        $form = json_encode($array);
        $client = new Client();
        $response = $client->post('http://localhost/backend/patient/create', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $form
        ]);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals($body[0], "record last_name only accepts letters");
    }

    //TEST 3 - VALIDATE PHONE NUMBER
    public function testPhoneNumber()
    {
        $form = $this->data("create");
        $array = json_decode($form);
        $array->phone_number = "8480l37g";
        $form = json_encode($array);
        $client = new Client();
        $response = $client->post('http://localhost/backend/patient/create', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $form
        ]);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals($body[0], "record phone_number error in format number");
    }

    //TEST 4 - VALIDATE EMAIL
    public function testValidateEmail()
    {
        $form = $this->data("create");
        $array = json_decode($form);
        $array->email = "yorlevarela21.com";
        $form = json_encode($array);
        $client = new Client();
        $response = $client->post('http://localhost/backend/patient/create', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $form
        ]);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals($body[0], "Email format invalid");
    }

    //TEST 5 - VALIDATE ID_CARD (CEDULA)
    public function testValidateIdCard()
    {
        $form = $this->data("create");
        $array = json_decode($form);
        $array->id_card = "1092903";
        $form = json_encode($array);
        $client = new Client();
        $response = $client->post('http://localhost/backend/patient/create', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $form
        ]);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals($body[0], "record id_card only accepts integers values and length 9 digit");
    }

    //TEST 6 - VALIDATE PAYER
    public function testValidatePayer()
    {
        $form = $this->data("create");
        $array = json_decode($form);
        $array->payer = "    ";
        $form = json_encode($array);
        $client = new Client();
        $response = $client->post('http://localhost/backend/patient/create', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $form
        ]);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals($body[0], "record payer only accepts letters");
    }
}
